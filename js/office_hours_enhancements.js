'use strict';

/* eslint strict: 0 */
(function (document, $) {

  // --------------------------------------------------------------------------
  // Variables
  // --------------------------------------------------------------------------
  var allRows = $('.office-hours-slot');
  var cardinality = $('form').attr('data-office-hours-cardinality');
  var validate = true;

  // --------------------------------------------------------------------------
  // Constructor/Setup/Init
  // --------------------------------------------------------------------------

  function init() {
    create();
  }

  setup();

  var rowsPrime = $('.ohi__slot').not('.ohi__slot-hide');
  var rowsHid = $('.ohi__slot-hide');

  function setup() {
    allRows.each(function (i, e) {
      $(e).addClass('ohi__slot');
      if (i % cardinality !== 0) {
        $(e).addClass('ohi__slot-hide');
      }
    });
  }

  function create() {
    createAddMoreButtons();
    createRemoveButton();
    createClosedButtons();
    createSpanClosed();
    setupDefault();
  }

  // --------------------------------------------------------------------------
  // Public Functions
  // --------------------------------------------------------------------------

  function createSpanClosed() {
    rowsPrime.each(function (i, e) {
      var r = $('<span/>').attr({
        class: 'span-closed'
      });
      r.text('Closed');
      $(e).find('td').last().append(r);
    });
  }

  function createRemoveButton() {
    allRows.each(function (i, e) {
      var r = $('<input/>').attr({
        type: 'button',
        class: 'del-btn',
        value: '- Shift'
      });
      $(e).find('td').last().append(r);
    });
  }

  function createAddMoreButtons() {
    allRows.each(function (i, e) {
      var r = $('<input/>').attr({
        type: 'button',
        class: 'add-btn',
        value: '+ Shift'
      });
      $(e).find('td').last().append(r);
    });
  }

  function createClosedButtons(r) {
    rowsPrime.each(function (i, e) {
      var r = $('<input/>').attr({
        type: 'checkbox',
        class: 'open-box',
        name: 'open-box'
      });
      $(e).find('td').first().prepend(r);
    });
  }

  function setupDefault() {
    rowsPrime.each(function (i, e) {
      $(e).find('.open-box').prop('checked', true);
    });

    rowsPrime.each(function (i, e) {
      $(e).find('td').last().find('.del-btn').hide();
      $(e).find('td').last().find('.span-closed').hide();
    });

    if (!(rowHasData(rowsPrime.first()))) {
      rowsPrime.first().find('.open-box').prop('checked', false);
      rowsPrime.first().find('td').not(':first').children().not('.del-btn').hide();
      rowsPrime.first().find('.span-closed').show();
    }
    if (!(rowHasData(rowsPrime.last()))) {
      rowsPrime.last().find('.open-box').prop('checked', false);
      rowsPrime.last().find('td').not(':first').children().not('.del-btn').hide();
      rowsPrime.last().find('.span-closed').show();
    }

    rowsHid.each(function (i, e) {
      $(e).find('td').first().text('');
      if (!(rowHasData($(e)))) {
        $(e).hide();
      }
    });

    allRows.each(function (i, e) {
      if (rowHasData($(e))) {
        // if current row is not hide
        if (!($(e).hasClass('ohi__slot-hide'))) {
          // and if current next row has data
          if (rowHasData($(e).next())) {
            // hide add-btn
            $(e).find('.add-btn').hide();
          }
        }
        else {
          if (!($(e).next().hasClass('ohi__slot-hide'))) {
            $(e).find('.add-btn').hide();
          }
        }
      }
      else {
        $(e).find('.open-box').prop('checked', false);
        $(e).find('td').not(':first').children().hide();
        $(e).find('.span-closed').show();
      }
    });
  }

  function resetRowInputs(row) {
    row.find('select').each(function (i, e) { $(e).prop('selectedIndex', 0); });
    row.find('input[type=text]').val('');
  }

  function rowHasData(row) {
    var total = 0;
    row.find('select').each(function (i, e) {
      if ($(e).prop('selectedIndex') !== 0) {
        total++;
      }
    });
    return total === 6;

  }

  // --------------------------------------------------------------------------
  // Event Handlers
  // --------------------------------------------------------------------------

  $(document).on('change', '.open-box', function () {
    if (!($(this).is(':checked'))) {
      var current = $(this).closest('tr').attr('data-drupal-selector');
      var start = current[current.length - 1];
      var end = +start + parseInt(cardinality) - 1;
      start++;
      for (var i = start; i <= end; i++) {
        var selector = '[data-drupal-selector=edit-field-office-hours-value-' + i + ']';
        $(this).closest('tr').siblings(selector).hide();
        resetRowInputs($(this).closest('tr').siblings(selector));
        resetRowInputs($(this).closest('tr'));
      }
    }

    $(this).parent().siblings().each(function (i, e) {
      if ($(e).children().css('display') !== 'none') {
        $(e).children().css('display', 'none');
      }
      else {
        $(e).children().not('div').css('display', 'table-cell');
        $(e).children().not('input').css('display', 'inline');
      }
    });

    $(this).closest('tr').siblings().last().find('.del-btn').hide();
    $(this).closest('td').siblings().last().find('.span-closed').toggle();
    $(this).closest('td').siblings().last().find('.del-btn').hide();
  });

  $(document).on('click', '.add-btn', function () {
    if (rowHasData($(this).closest('tr'))) {
      if ($(this).closest('tr').next().hasClass('ohi__slot-hide')) {
        $(this).closest('tr').next().toggle();
        $(this).closest('tr').next().find('div').show();
        if (!($(this).closest('tr').next().next().hasClass('ohi__slot-hide'))) {
          $(this).closest('tr').next().find('.add-btn').hide();
        }
        $(this).hide();
      }
    }
    else {
      if (validate) {
        alert('You must define a shift before adding a new one.');
      }
      else {
        if ($(this).closest('tr').next().hasClass('ohi__slot-hide')) {
          $(this).closest('tr').next().toggle();
          if (!($(this).closest('tr').next().next().hasClass('ohi__slot-hide'))) {
            $(this).closest('tr').next().find('.add-btn').hide();
          }
          $(this).hide();
        }
      }
    }

  });

  $(document).on('click', '.del-btn', function () {
    $(this).parent().find('.add-btn').show(); // Maybe still needs to be toggle?
    $(this).closest('tr').hide();
    $(this).closest('tr').prev().find('.add-btn').show();
    resetRowInputs($(this).closest('tr'));
  });

  // --------------------------------------------------------------------------
  // Initialization
  // --------------------------------------------------------------------------

  init();

})(document, jQuery);
