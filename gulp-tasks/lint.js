/**
 * @file
 * Task: Lint: Scripts.
 */

'use strict';

module.exports = function (gulp, plugins, options) {

  // Lint Sass.
  gulp.task('lint:sass', function () {
    return gulp.src(options.linting.files.sass)
      .pipe(plugins.sassLint())
      .pipe(plugins.sassLint.format());
  });

  gulp.task('lint:sass-with-fail', function () {
    return gulp.src(options.linting.files.sass)
      .pipe(plugins.sassLint())
      .pipe(plugins.sassLint.format())
      .pipe(plugins.sassLint.failOnError());
  });

  // Lint JavaScript.
  gulp.task('lint:js', function () {
    return gulp.src(options.linting.files.js)
      .pipe(plugins.eslint())
      .pipe(plugins.eslint.format());
  });

  gulp.task('lint:js-gulp', function () {
    return gulp.src(options.linting.files.gulp)
      .pipe(plugins.eslint({
        useEslintrc: true,
        ecmaFeatures: {
          modules: true,
          module: true
        },
        env: {
          mocha: true,
          node: true,
          es6: true
        }
      }))
      .pipe(plugins.eslint.format());
  });

  // Lint JavaScript and throw an error for a CI to catch.
  gulp.task('lint:js-with-fail', function () {
    return gulp.src(options.linting.files.js)
      .pipe(plugins.eslint())
      .pipe(plugins.eslint.format())
      .pipe(plugins.eslint.failOnError());
  });
};
