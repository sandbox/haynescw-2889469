/**
 * @file
 * Task: Watch.
 */

'use strict';

module.exports = function (gulp, plugins, options) {

  gulp.task('watch', ['watch:sass', 'watch:js', 'watch:twig']);

  gulp.task('watch:js', function () {
    return gulp.watch([
      options.js.files
    ], function () {
      plugins.runSequence(
        'lint:js',
        'minify:js',
        'browser-sync:reload'
      );
    });
  });

  gulp.task('watch:sass', function () {
    return gulp.watch([
      options.sass.files
    ], function () {
      plugins.runSequence(
        'lint:sass',
        'compile:sass',
        'minify:css',
        'browser-sync:reload'
      );
    });
  });

  gulp.task('watch:twig', function () {
    return gulp.watch([
      options.twig.files
    ], function () {
      plugins.runSequence(
        'browser-sync:reload'
      );
    });
  });
};
