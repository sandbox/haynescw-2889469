/**
 * @file
 * Task: Minify.
 */

'use strict';

module.exports = function (gulp, plugins, options) {

  gulp.task('minify:css', function () {
    return gulp.src([
      options.css.files,
      '!' + options.css.destination + '**/*.min.css'
    ])
      .pipe(plugins.rename({
        suffix: '.min'
      }))
      .pipe(plugins.cleanCSS({ compatibility: 'ie8' }))
      .pipe(gulp.dest(options.css.destination));
  });

  gulp.task('minify:js', function () {
    return gulp.src([
      options.js.files,
      '!' + options.js.destination + '**/*.min.css'
    ])
      .pipe(plugins.rename({
        suffix: '.min'
      }))
      .pipe(plugins.uglify())
      .pipe(gulp.dest(options.js.destination));
  });

  gulp.task('minify:images', function () {
    return gulp.src([
      options.images.files
    ])
      .pipe(plugins.imagemin({
        progressive: true,
        multipass: true
      }))
      .pipe(gulp.dest(options.images.destination));
  });

};
