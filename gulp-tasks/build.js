/**
 * @file
 * Task: Build.
 */

'use strict';

module.exports = function (gulp, plugins, options) {

  gulp.task('build', function (cb) {
    // Run linting last, otherwise its output gets lost.
    plugins.runSequence(
      'clean',
      'compile:sass',
      ['minify:css', 'minify:js', 'minify:images'],
      ['lint:js-gulp', 'lint:js-with-fail', 'lint:sass-with-fail'],
      cb
    );
  });

  gulp.task('build:dev', function (cb) {
    // Run linting last, otherwise its output gets lost.
    plugins.runSequence(
      'clean',
      'compile:sass',
      ['minify:css', 'minify:js', 'minify:images'],
      ['lint:js-gulp', 'lint:js', 'lint:sass'],
      cb
    );
  });
};
