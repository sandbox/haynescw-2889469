/**
 * @file
 * Task: Browsersync.
 */

'use strict';

module.exports = function (gulp, plugins, options) {

  var browserSync = plugins.browserSync.create();

  gulp.task('browser-sync', function () {
    browserSync.init(options.browserSync);
  });

  gulp.task('browser-sync:reload', function () {
    browserSync.reload();
  });
};
