/**
 * @file
 * Task: Serve.
 */

'use strict';

module.exports = function (gulp, plugins, options) {

  gulp.task('serve', ['build:dev', 'browser-sync', 'watch']);
};
