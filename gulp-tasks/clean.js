/**
 * @file
 * Task: Clean.
 */

'use strict';

module.exports = function (gulp, plugins, options) {

  // Clean CSS files.
  gulp.task('clean:css', function () {
    plugins.del.sync([
      options.css.destination
    ]);
  });

  gulp.task('clean:js', function () {
    plugins.del.sync([
      options.js.destination
    ]);
  });

  gulp.task('clean:images', function () {
    plugins.del.sync([
      options.images.destination
    ]);
  });

  gulp.task('clean', ['clean:css', 'clean:js', 'clean:images']);

};
