// -------------------------------------
//
//   Gulpfile
//
// -------------------------------------
//
// Available tasks:
//   `gulp`
//   `gulp build`
//   `gulp build:dev`
//   `gulp clean`
//   `gulp clean:css`
//   `gulp clean:js`
//   `gulp compile:sass`
//   `gulp lint:js`
//   `gulp lint:sass`
//   `gulp minify:css`
//   `gulp minify:js`
//   `gulp serve`
//   `gulp watch`
//   `gulp watch:js`
//   `gulp watch:sass`
//
// -------------------------------------


// -------------------------------------
//   Modules
// -------------------------------------
//
// gulp              : The streaming build system
// gulp-autoprefixer : Prefix CSS
// gulp-concat       : Concatenate files
// gulp-clean-css    : Minify CSS
// gulp-eslint       : JavaScript code quality tool
// gulp-load-plugins : Automatically load Gulp plugins
// gulp-plumber      : Prevent pipe breaking from errors
// gulp-rename       : Rename files
// gulp-sass         : Compile Sass
// gulp-sass-glob    : Provide Sass Globbing
// gulp-sass-lint    : Lint Sass
// gulp-sourcemaps   : Generate sourcemaps
// gulp-imagemin     : Minify image assets
// gulp-uglify       : Minify JavaScript with UglifyJS
// gulp-util         : Utility functions
// gulp-watch        : Watch stream
// browser-sync      : Device and browser testing tool
// del               : delete
// run-sequence      : Run a series of dependent Gulp tasks in order
// -------------------------------------

'use strict';

var gulp = require('gulp');

// Setting pattern this way allows non gulp- plugins to be loaded as well.
var plugins = require('gulp-load-plugins')({
  pattern: '*',
  rename: {
    'browser-sync': 'browserSync',
    'gulp-sass-glob': 'sassGlob',
    'run-sequence': 'runSequence',
    'gulp-clean-css': 'cleanCSS'
  }
});

// These are used in the options below.
var paths = {
  styles: {
    source: 'sass/',
    destination: 'dist/css/'
  },
  scripts: {
    source: 'js/',
    destination: 'dist/js/'
  },
  images: {
    source: 'images/',
    destination: 'dist/images'
  },
  templates: 'templates/'
};

// These are passed to each task.
var options = {

  // ----- Browsersync ----- //

  browserSync: {
    proxy: {
      target: 'http://local.opa.com/'
    },
    port: '8080',
    ghostMode: {
      clicks: false,
      forms: false,
      scroll: true
    },
    open: 'external',
    xip: true,
    logConnections: true
  },

  // ----- CSS ----- //

  css: {
    files: paths.styles.destination + '**/*.css',
    destination: paths.styles.destination
  },

  // ----- Sass ----- //

  sass: {
    files: paths.styles.source + '**/*.scss',
    destination: paths.styles.destination,
    includePaths: [
      'node_modules/normalize.scss'
    ]
  },

  // ----- Twig ----- //

  twig: {
    files: paths.templates + '**/*.html.twig',
    destination: paths.templates
  },

  // ----- JS ----- //
  js: {
    files: paths.scripts.source + '**/*.js',
    destination: paths.scripts.destination
  },

  // ----- Images ----- //

  images: {
    files: paths.images.source + '**/*.{png,gif,jpg,svg}',
    destination: paths.images.destination
  },

  // ----- Linting ----- //

  linting: {
    files: {
      sass: paths.styles.source + '**/*.scss',
      js: paths.scripts.source + '**/*.js',
      gulp: [
        'gulpfile.js',
        'gulp-tasks/**/*'
      ]
    }

  }

};

// Tasks
require('./gulp-tasks/browser-sync')(gulp, plugins, options);
require('./gulp-tasks/build')(gulp, plugins, options);
require('./gulp-tasks/clean')(gulp, plugins, options);
require('./gulp-tasks/compile-sass')(gulp, plugins, options);
require('./gulp-tasks/default')(gulp, plugins, options);
require('./gulp-tasks/lint')(gulp, plugins, options);
require('./gulp-tasks/minify')(gulp, plugins, options);
require('./gulp-tasks/serve')(gulp, plugins, options);
require('./gulp-tasks/watch')(gulp, plugins, options);
